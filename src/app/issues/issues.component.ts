import { Component, OnInit, Input } from '@angular/core';
import { IssuesService } from './issues.service';
import { Issue } from '../models/Issue.model';

@Component({
  selector: 'issues',
  styleUrls: ['./issues.component.scss'],
  template: `
    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <span> Listado de incidencias de repositorios Github </span>
      </mat-toolbar-row>
    </mat-toolbar>
    <div class="repository-form">
      <H1>Inserta el repositorio y obten las incidencias:</H1>
      <form name="issues">
        <mat-form-field class="repository-form-input">
          <input matInput placeholder="Repositorio" [(ngModel)]="repositoryName" name="repositoryName">
        </mat-form-field>
        <button mat-raised-button
            class="repository-form-submit"
            color="primary"
            (click)="getIssuesOfRepository(repositoryName)">
          OBTENER INCIDENCIAS
        </button>
      </form>

      <issues-list
          [issues]="issues"
      ></issues-list>

      <mat-spinner *ngIf="gettingIssues" class="gettingIssuesSpinner"></mat-spinner>
    </div>
  `
})
export class IssuesComponent {
  public repositoryName: 'vuejs/vue-cli';
  public issues: Issue[];
  public gettingIssues = false;

  constructor(
    private issuesService: IssuesService
  ) { }

  getIssuesOfRepository(repository: string) {
    if (this.gettingIssues) {
      return;
    }

    this.gettingIssues = true;
    this.issuesService.getIssuesOfRepository(repository)
      .subscribe(issues => {
        this.gettingIssues = false;
        this.issues = issues;
      });
  }

}
