import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/shared/config.service';
import { Issue } from '../models/Issue.model';
import { Observable } from 'rxjs';

@Injectable()
export class IssuesService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
  ) { }

  getIssuesOfRepository(repository: string): Observable<Issue[]> {
    const url = `${this.configService.gitBaseUrl}/${repository}/issues`;

    return this.http.get<Issue[]>(url);
  }

  // TODO: sirve para cualquier url
  getComentsOfIssue(issue: Issue): Observable<Issue[]> {
    const url = `${issue.comments_url}`;

    return this.http.get<Issue[]>(url);
  }

}
