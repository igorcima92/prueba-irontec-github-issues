import { Component, Input } from '@angular/core';
import { Issue } from '../../models/Issue.model';

@Component({
  selector: 'issues-list',
  styleUrls: ['./issues-list.component.scss'],
  template: `
    <issue
        *ngFor="let issue of issues"
        [issue]="issue"
    ></issue>
  `
})
export class IssuesListComponent {
  @Input() issues: Issue[];

  constructor(
  ) { }

}
