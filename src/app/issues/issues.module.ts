import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { IssuesListComponent } from 'src/app/issues/issues-list/issues-list.component';
import { IssuesService } from 'src/app/issues/issues.service';
import { IssuesComponent } from 'src/app/issues/issues.component';
import { IssueComponent } from 'src/app/issues/issue/issue.component';
import { IssueCommentComponent } from 'src/app/issues/issue/issue-comment/issue-comment.component';


const appRoutes: Routes = [
  { path: '', component: IssuesComponent },
];

@NgModule({
  declarations: [
    IssuesComponent,
    IssuesListComponent,
    IssueComponent,
    IssueCommentComponent,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
    ),
    SharedModule,
    RouterModule,
    BrowserModule,
  ],
  providers: [
    IssuesService
  ],
  bootstrap: [IssuesComponent]
})

export class IssuesModule { }
