import { TestBed, async } from '@angular/core/testing';
import { IssuesComponent } from './issues.component';
import { IssueComponent } from 'src/app/issues/issue/issue.component';
import { IssuesService } from 'src/app/issues/issues.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { IssuesListComponent } from './issues-list/issues-list.component';
import { of } from 'rxjs';

class IssuesServiceMock {
  getIssuesOfRepository() { }
}

describe('IssuesComponent', () => {
  let fixture;
  let app;
  let issuesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        IssuesListComponent,
        IssuesComponent,
        IssueComponent,
      ],
      imports: [
      ],
      providers: [
        {provide: IssuesService, usevalue: IssuesServiceMock}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(IssuesComponent);
    issuesService = TestBed.get(IssuesService);
    app = fixture.debugElement.componentInstance;
  }));

  it('Dont call "getIssuesOfRepository" method of issuesService if it is getting data', async(() => {
    const incrementSpy = spyOn(issuesService, 'getIssuesOfRepository').and.returnValue(of([]));

    app.getIssuesOfRepository();
    app.getIssuesOfRepository();

    expect(incrementSpy).toHaveBeenCalledTimes(1);
  }));


});
