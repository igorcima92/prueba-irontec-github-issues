import { Component, Input, OnInit } from '@angular/core';
import { IssueComment } from 'src/app/models/issueComent.model';

@Component({
  selector: 'issue-comment',
  styleUrls: ['./issue-comment.component.scss'],
  template: `
    <mat-card class="issue">
      <mat-card-header>
        <div mat-card-avatar
            class="header-image"
            [ngStyle]="{'background-image': 'url(' + issueComment.user.avatar_url + ')'}">
        </div>
        <mat-card-title>{{issueComment.id}}</mat-card-title>
        <mat-card-subtitle>{{issueComment.user.login}}</mat-card-subtitle>
        <div class="moreInfo">
          <p>Creado: {{created_at | date}}</p>
          <p>Actualizado: {{updated_at | date}}</p>
        </div>
      </mat-card-header>
      <mat-card-content>
        <p [innerHTML]="issueComment.body">
        </p>
      </mat-card-content>

    </mat-card>
  `
})
export class IssueCommentComponent implements OnInit {
  @Input() issueComment: IssueComment;
  updated_at: Date;
  created_at: Date;

  constructor() {
  }

  ngOnInit(): void {
    this.transformDates();
  }

  transformDates() {
    this.updated_at = new Date(this.issueComment.updated_at);
    this.created_at = new Date(this.issueComment.created_at);
  }
}
