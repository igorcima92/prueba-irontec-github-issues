import { Component, OnInit, Input } from '@angular/core';
import { Issue } from '../../models/Issue.model';
import { IssuesService } from '../issues.service';
import { IssueComment } from 'src/app/models/issueComent.model';

@Component({
  selector: 'issue',
  styleUrls: ['./issue.component.scss'],
  template: `
    <mat-card class="issue">
      <mat-card-header>
        <div mat-card-avatar
            class="header-image"
            [ngStyle]="{'background-image': 'url(' + issue.user.avatar_url + ')'}">
        </div>
        <mat-card-title>{{issue.id}}</mat-card-title>
        <mat-card-subtitle>{{issue.user.login}}</mat-card-subtitle>
        <div class="moreInfo">
          <p>Creado: {{created_at | date}}</p>
          <p>Actualizado: {{updated_at | date}}</p>
        </div>
      </mat-card-header>
      <mat-card-content>
        <p [innerHTML]="issue.body">
        </p>
      </mat-card-content>
      <mat-card-actions *ngIf="issue.comments !== 0">
        <button mat-button (click)="getComments()">Mostrar comentarios</button>
      </mat-card-actions>

      <div class="comments">
        <issue-comment *ngFor="let issueComment of issueComments" [issueComment]="issueComment"></issue-comment>
        <mat-spinner *ngIf="gettingComments" class="getCommentsSpinner"></mat-spinner>
      </div>
     

    </mat-card>
  `
})
export class IssueComponent implements OnInit {
  @Input() issue: Issue;
  updated_at: Date;
  created_at: Date;
  issueComments: IssueComment[];
  gettingComments = false;

  constructor(
    private issuesService: IssuesService
  ) { }

  ngOnInit(): void {
    this.transformDates();
  }

  transformDates() {
    this.updated_at = new Date(this.issue.updated_at);
    this.created_at = new Date(this.issue.created_at);
  }

  getComments() {
    if (this.gettingComments) {
      return ;
    }

    this.gettingComments = true;
    this.issuesService.getComentsOfIssue(this.issue)
      .subscribe(issueComments => {
        this.gettingComments = false;
        this.issueComments = issueComments;
      });
  }
}
