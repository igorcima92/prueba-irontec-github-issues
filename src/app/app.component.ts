import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  template: `
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  public title = 'Obtener git issues';

  constructor() {

  }


}
