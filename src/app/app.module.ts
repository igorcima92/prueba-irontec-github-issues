import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core.module';
import { IssuesModule } from 'src/app/issues/issues.module';

import { AppComponent } from 'src/app/app.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'issues', pathMatch: 'full'},
  { path: 'issues', loadChildren: 'app/issues/issues.module@IssuesModule' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    CoreModule,
    SharedModule,
    RouterModule,
    BrowserModule,
    IssuesModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
