import { Injectable } from '@angular/core';
import { config } from 'src/config/config';

@Injectable()
export class ConfigService {
  public gitBaseUrl: string;

  constructor() {
    this.gitBaseUrl = config.gitBaseUrl;
  }
}
