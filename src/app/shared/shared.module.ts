import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
        MatButtonModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatGridListModule,
        MatInputModule,
        MatCardModule,
        MatProgressSpinnerModule,
      } from '@angular/material';


@NgModule({
  exports: [
    TranslateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatGridListModule,
    MatInputModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatCardModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ]
})

export class SharedModule { }
