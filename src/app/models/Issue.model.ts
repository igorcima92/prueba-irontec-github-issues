import { User } from 'src/app/models/user.model';

export interface Issue {
  id: number;
  number: number;
  created_at: string;
  updated_at: string;
  comments: number;
  comments_url: string;
  user: User;
}
