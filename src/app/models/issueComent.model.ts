import { User } from 'src/app/models/user.model';

export interface IssueComment {
  id: number;
  number: number;
  created_at: string;
  updated_at: string;
  user: User;
}
