import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ConfigService } from 'src/app/shared/config.service';

@NgModule({
  imports: [
    HttpClientModule,
  ],
  providers: [
    ConfigService,
  ]
})

export class CoreModule { }
